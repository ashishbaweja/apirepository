$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/cucumberapi/apitestautomation/functionaltests/Petstore.feature");
formatter.feature({
  "name": "Test Demo Pet Store API\u0027s \"GET\", \"POST\", \"PUT\" and \"DELETE\" functionality.",
  "description": "Description: The Purpose of this test is to verify if user can get all available pets, add a new pet, update status of a pet, delete a pet on Demo Pet Store.",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User gets all pets, adds a new pet, update newly added pet, deletes same pet",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User gets all available pets",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.user_gets_all_available_pets()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be able to retrive all available pets from Pet Store",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.user_should_be_able_to_retrive_all_available_pets_from_Pet_Store()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User adds a pet \"TestPet2\" to Demo Pet Store",
  "keyword": "And "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.user_adds_a_pet_to_Demo_Pet_Store(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "pet \"TestPet2\" should be added successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.pet_should_be_added_successfully(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User upates pet \"TestPet2\" status to \"sold\"",
  "keyword": "And "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.user_upates_pet_status_to(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "status of pet should be updated to \"sold\" successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.status_of_pet_should_be_updated_to_successfully(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User deletes pet \"TestPet2\"",
  "keyword": "And "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.user_deletes_pet(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "pet should be deleted successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumberapi.apitestautomation.stepDefinitions.APISteps.pet_should_be_deleted_successfully()"
});
formatter.result({
  "status": "passed"
});
});