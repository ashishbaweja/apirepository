package cucumberapi.apitestautomation.managers;

import cucumberapi.apitestautomation.APITests.DeletePet;
import cucumberapi.apitestautomation.APITests.GetPets;
import cucumberapi.apitestautomation.APITests.PostPet;
import cucumberapi.apitestautomation.APITests.RequestBodyUtility;
import cucumberapi.apitestautomation.APITests.UpdatePet;


public class ObjectManager {
	
	GetPets getpets;
	PostPet postpet;
	UpdatePet updatepet;
	DeletePet deletepet;
	RequestBodyUtility requtil;
	
	public GetPets getGetPet()
	{
		return (getpets==null)? getpets=new GetPets() : getpets;			
	}
	
	public PostPet getPostPet()
	{
		return (postpet==null)? postpet=new PostPet() : postpet;			
	}
	
	public UpdatePet getUpdatePet()
	{
		return (updatepet==null)? updatepet=new UpdatePet() : updatepet;			
	}

	public DeletePet getdeletepet()
	{
		return (deletepet==null)? deletepet=new DeletePet() : deletepet;			
	}
	
	public RequestBodyUtility getReqBody()
	{
		return (requtil==null)? requtil=new RequestBodyUtility() : requtil;			
	}

}
