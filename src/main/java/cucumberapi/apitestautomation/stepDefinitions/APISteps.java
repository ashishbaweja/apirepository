package cucumberapi.apitestautomation.stepDefinitions;

import java.io.IOException;

import cucumberapi.apitestautomation.APITests.DeletePet;
import cucumberapi.apitestautomation.APITests.GetPets;
import cucumberapi.apitestautomation.APITests.PostPet;
import cucumberapi.apitestautomation.APITests.RequestBodyUtility;
import cucumberapi.apitestautomation.APITests.UpdatePet;
import cucumberapi.apitestautomation.managers.ObjectManager;
import io.cucumber.java.en.*;

public class APISteps {

	private ObjectManager objectmgr = new ObjectManager();;
	private GetPets  getpets;
	private PostPet  postpet;
	private UpdatePet updatepet;
	private DeletePet deletepet;
	private RequestBodyUtility requtil;
	private long id;
	
	@Given("User gets all available pets")
	public void user_gets_all_available_pets() throws IOException {
		
		 getpets =objectmgr.getGetPet();
		 getpets.getavailablepets();
	}

	@Then("User should be able to retrive all available pets from Pet Store")
	public void user_should_be_able_to_retrive_all_available_pets_from_Pet_Store() {
		getpets.validateAvailablePetsAreRetrieved();  
	}

	@Given("User adds a pet {string} to Demo Pet Store")
	public void user_adds_a_pet_to_Demo_Pet_Store(String petname) throws IOException{
		postpet=objectmgr.getPostPet();
		requtil=objectmgr.getReqBody();
		String reqbody=requtil.CreatePostRequestBody(petname);
		postpet.postapet(reqbody);
	}

	@Then("pet {string} should be added successfully")
	public void pet_should_be_added_successfully(String petname) {
	 postpet.validatePetAddedSuccessfully(petname);

	}


	@Given("User upates pet {string} status to {string}")
	public void user_upates_pet_status_to(String petname, String status) throws IOException{
		updatepet=objectmgr.getUpdatePet();
		requtil=objectmgr.getReqBody();
		postpet=objectmgr.getPostPet();
		id=postpet.getNewlyAddedPet();
		String reqbody=requtil.CreatePutRequestBody(id,petname,status);
		updatepet.updatepet(reqbody);

	}
	
	@Then("status of pet should be updated to {string} successfully")
	public void status_of_pet_should_be_updated_to_successfully(String status) {
		updatepet.validatePetUpdatedSuccessfully(status);
	}
	
	@Given("User deletes pet {string}")
	public void user_deletes_pet(String string) throws IOException {
		deletepet=objectmgr.getdeletepet();
		postpet=objectmgr.getPostPet();
		id=postpet.getNewlyAddedPet();
		deletepet.deletepet(id);
	}
	
	@Then("pet should be deleted successfully")
	public void pet_should_be_deleted_successfully() {
	    deletepet.validatePetIsDeleted();
	}




}
