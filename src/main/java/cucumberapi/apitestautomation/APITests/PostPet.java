package cucumberapi.apitestautomation.APITests;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;

import cucumberapi.apitestautomation.utility.CommonUtility;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PostPet extends CommonUtility{
	

	Response response;
	String jsonString;
	String URL;
	Properties prop;
	Logger Log = Logger.getLogger("PostPet");
	
	
	public void postapet(String PostPetReqbody) throws IOException
	{
		DOMConfigurator.configure("log4j.xml");
		prop = ReadConfigData();
	    URL=prop.getProperty("URL");
	    response = RestAssured.given().when().body(PostPetReqbody).contentType("application/json").post(URL+"/pet");
	}

	public void validatePetAddedSuccessfully(String petname) 
	{
		 jsonString = response.asString();
		 String pets = JsonPath.from(jsonString).get("name");
		 Assert.assertTrue(pets.contentEquals(petname));
		 Log.info("Pet inserted successfully");
	     response.then().assertThat().statusCode(200);
	   
	}
	
	public long getNewlyAddedPet()
	{ 
     jsonString = response.asString();
	 long id = JsonPath.from(jsonString).get("id");
	 Log.info("Pet ID retrieved "+id);
     return id;
	}

}
