package cucumberapi.apitestautomation.APITests;

public class RequestBodyUtility 
{
	
	public String CreatePostRequestBody( String value)
	{	
		String	CreatedReqbody= "{  \"id\": 0,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \""+  value   +"\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}";	
		return CreatedReqbody;
	}
	

	public String CreatePutRequestBody(long id, String name, String status)
	{
		String	CreatedReqbody= "{  \"id\":" +id+",  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \""  +name+   "\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \""  +status+  "\"}";
		return CreatedReqbody;
	}
	

}
