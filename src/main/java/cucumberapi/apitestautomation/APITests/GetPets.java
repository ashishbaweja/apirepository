package cucumberapi.apitestautomation.APITests;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;

import cucumberapi.apitestautomation.utility.CommonUtility;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class GetPets extends CommonUtility{
	
	Response response;
	String URL;
	Properties prop;
	Logger Log = Logger.getLogger("GetsPets");

	
	public void getavailablepets() throws IOException 
	{
		DOMConfigurator.configure("log4j.xml");
		prop = ReadConfigData();
	    URL=prop.getProperty("URL");
	    response = RestAssured.given().when().get(URL+"/pet/findByStatus?status=available");
	}

	public void validateAvailablePetsAreRetrieved() 
	{
		 String jsonString = response.asString();
		 List<String> pets = JsonPath.from(jsonString).getList("status");	 
		 Assert.assertTrue(pets.size() > 0);
		 Assert.assertTrue(pets.contains("available"));
         response.then().assertThat().statusCode(200);
         Log.info("All available pets retrieved successfully");
		
	}

}
