package cucumberapi.apitestautomation.APITests;


import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;

import cucumberapi.apitestautomation.utility.CommonUtility;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class UpdatePet extends CommonUtility{
	

	Response response;
	String URL;
	Properties prop;
	Logger Log = Logger.getLogger("UpdatePet");
	
	
	public void updatepet(String UpdatePetReqbody) throws IOException
	{
		DOMConfigurator.configure("log4j.xml");
		prop = ReadConfigData();
	    URL=prop.getProperty("URL");
	    response = RestAssured.given().when().body(UpdatePetReqbody).contentType("application/json").post(URL+"/pet");
	}

	public void validatePetUpdatedSuccessfully(String status) 
	{
		 String jsonString = response.asString();
		 String Status = JsonPath.from(jsonString).get("status");	 
		 Assert.assertTrue(Status.contentEquals(status));
	     response.then().assertThat().statusCode(200);
	     Log.info("pet updated successfully to sold");
	}

}
