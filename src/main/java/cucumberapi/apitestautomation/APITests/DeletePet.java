package cucumberapi.apitestautomation.APITests;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;

import cucumberapi.apitestautomation.utility.CommonUtility;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class DeletePet extends CommonUtility{
	
	Response response;
	String URL;
	Properties prop;
	Logger Log = Logger.getLogger("DeletePet");
	long id;
	
	public void deletepet(long id) throws IOException
	{
		DOMConfigurator.configure("log4j.xml");
		prop = ReadConfigData();
	    URL=prop.getProperty("URL");
		this.id=id;
		String s=String.valueOf(id);
	    response = RestAssured.given().when().delete(URL+"/pet/"+s);
	}

	public void validatePetIsDeleted() 
	{
		 String jsonString = response.asString();
		 String IDRecieved = JsonPath.from(jsonString).get("message");	
		 long longID=Long.parseLong(IDRecieved); 
		 Assert.assertTrue(longID==id);
         response.then().assertThat().statusCode(200);
         Log.info("pet deleted successfully");
		
	}

}
