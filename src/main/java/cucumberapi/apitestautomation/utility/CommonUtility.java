package cucumberapi.apitestautomation.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CommonUtility {
	
Properties prop;

public Properties ReadConfigData() throws IOException 
{
prop = new Properties();
FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\test\\java\\cucumberapi\\apitestautomation\\configuration\\config.properties"));
prop.load(fis);
fis.close();	
return prop;
}	

}
