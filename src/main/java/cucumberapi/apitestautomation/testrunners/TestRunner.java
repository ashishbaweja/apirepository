package cucumberapi.apitestautomation.testrunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
 
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/cucumberapi/apitestautomation/functionaltests",
        glue = {"cucumberapi.apitestautomation.stepDefinitions"},
        plugin = {"html:target/site/cucumber-pretty","json:target/cucumber.json"},
        monochrome = true,
        strict = true
)
 
public class TestRunner {
 
}