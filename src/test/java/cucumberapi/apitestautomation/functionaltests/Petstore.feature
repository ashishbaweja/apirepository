Feature: Test Demo Pet Store API's "GET", "POST", "PUT" and "DELETE" functionality.
Description: The Purpose of this test is to verify if user can get all available pets, add a new pet, update status of a pet, delete a pet on Demo Pet Store.

Scenario: User gets all pets, adds a new pet, update newly added pet, deletes same pet
Given User gets all available pets 
Then User should be able to retrive all available pets from Pet Store
And User adds a pet "TestPet2" to Demo Pet Store
Then pet "TestPet2" should be added successfully
And User upates pet "TestPet2" status to "sold"
Then status of pet should be updated to "sold" successfully
And User deletes pet "TestPet2" 
Then pet should be deleted successfully